// MIXINS
//Dummy Variable
$x = false;
// The top, left, right, and bottom are optional
setup(display, position, margin, top = null, right = null, bottom = null, left = null)  
    display display
    position position
    margin margin
    top top
    left left
    right right
    bottom bottom
        
font(font-family, font-size, letter-spacing, font-weight, line-height = 1)
    font-family font-family
    font-size font-size
    letter-spacing letter-spacing
    font-weight font-weight
    line-height line-height
        
// GENERAL FONTS AND CLASSES AND SETUP STUFF
* {box-sizing: border-box; transition: 0.35s ease;}
    
.rela-block
    setup(block, relative, auto)
.rela-inline
    setup(inline-block, relative, auto)
.floated
    setup(inline-block, relative, $x)
    float left
    
.abs-center
    setup($x, absolute, $x, 50%, $x, $x, 50%)
    transform translate(-50%, -50%)
    text-align center
    width 88%
    
// --- COLORS ---
$topColor = #272727;
$logoColor = #E0E5F5;

// --- PAGE STYLINGS ---
body {font('Open Sans',18px,0px,400,26px); color: black}
p.light {color: #727272}
h1 {font('Raleway',54px,10px,200,60px); text-transform: uppercase;}
h2 {font('Montserrat',24px,6px,200,30px); text-transform: uppercase; margin-bottom: 15px;}
.spacer {height: 400px}
       
.menu-container
    setup($x,fixed,$x,0,0,0,0)
    z-index -1
    opacity 0
    background-color rgba(0,0,0,0.7)
    transition 0.2s ease
    &.displayed {z-index: 200; opacity: 1; transition: 0.35s ease, opacity 0.5s ease;};
    &.displayed .menu-tab {top: 50%}
    & .close {color: white; margin: 30px; cursor: pointer;}
    
    
.side-nav
    setup($x,fixed,$x,1px,$x,$x,0)
    z-index 100
    height 80px
    width 80px
    opacity 0
    overflow hidden
    transition 0.35s ease, height 0s 0.35s ease, width 0.35s ease
    &.nav-open
        height 400px
        width 300px
        opacity 1
        background-color #F4F4F9
        box-shadow 2px 4px 26px 2px rgba(0,0,0,0.35)
        transition 0.35s ease, height 0.4s 0.35s cubic-bezier(0.85,0,0.15,1), width 0.4s 0.75s cubic-bezier(0.85,0,0.15,1)
        
.nav-header
    top -20%
    left 48.5%
    transition 0.35s ease, top 0s 0.2s ease
    &.nav-open 
        top 11%
        transition 0.35s ease, top 0.4s 1s cubic-bezier(0.85,0,0.15,1)
        
.nav-options
    width 100%
    height auto
    top calc(50% + 5px)
    & li
        width 70%
        padding 12px 0px
        cursor pointer
        left -120%
        &:hover {color: #F67F74}
    &.nav-open li{left: -2%;}
        
for num in (1..10)
    .nav-options.nav-open li:nth-child({num})
        transition 0.35s ease, left 0.35s (0.9s + (0.05s * (num - 1))) cubic-bezier(0, 0, 0.15, 1.3)
            
.top-section
    height 640px
    color $logoColor
    background-color $topColor
    overflow hidden

.header-container
    height 400px 
    width 400px
    border 4px solid
    top 47%
    border-radius 100%
    &:before
        content ""
        setup($x,absolute,$x,-12px,-12px,-12px,-12px)
        border 2px solid $logoColor
        border-radius 100%
    
.food-header
    background-color $topColor
    font('Mr Dafoe', 90px ,0px, 400, 106px)
    width 480px
    text-transform none
    top 60%
    left 52%
    transform translate(-50%,-50%) rotate(-8deg)
    &:first-letter {letter-spacing: -6px}
    &:before, &:after
        content ""
        setup($x,absolute,$x,$x,0,$x,0)
        height 4px
        background-color $logoColor
        left 0
    &:before {top: 8px}
    &:after {bottom: 8px}
        
.eye
    top 18%
    height 12px
    width 12px
    background-color $logoColor
    border-radius 100%
    &.left {left: 35%}
    &.right {left: 65%}
        
.mouth
    width 140px
    height 72px
    top 31%
    border-radius 10px 10px 700px 700px
    border 3px solid $logoColor
        
.extra-lines
    setup($x,absolute,$x,46%,$x,$x,14%)
    transform rotate(-8deg)
    width 280px
    border-bottom 2px solid
    &.bottom
        top 74%
        left 18%
        transform rotate(-188deg)
        
.nav-button, .search-button
    z-index 100
    setup($x,fixed,$x,25px)
    height 30px
    width 30px
    cursor pointer
.nav-button {left: 25px}
.nav-button:hover .nav-bars
    background-color white
    &:before, &:after
        background-color white
.nav-button.nav-open
    left 245px
    transition 0.35s ease, left 0.4s 0.75s cubic-bezier(0.85,0,0.15,1)
.nav-button.nav-open, .nav-button.black
    & .nav-bars
        background-color $topColor
        &:before, &:after
            background-color $topColor
.search-button {right: 25px}
.search-button:hover .magnefying-glass
    &:before {border-color: white}
    &:after {background-color: white}
.search-button.black .magnefying-glass
    &:before {border-color: $topColor}
    &:after {background-color: $topColor}
    
.nav-bars
    width 19px
    height 2px
    background-color #BBB
    left 40%
    &:before, &:after
        content ""
        setup($x,absolute,$x,$x,$x,$x,0)
        width 130%
        height 100%
        background-color #BBB
        transition 0.35s ease
    &:before {top: -7px}
    &:after {top: 7px; width: 70%}
        
.magnefying-glass
    height 30px
    width 30px
    &:before
        content ""
        setup($x,absolute,$x,3px,3px)
        height 15px
        width 15px
        border 2px solid #BBB
        border-radius 100%
        transition 0.35s ease
    &:after
        content ""
        setup($x,absolute,$x,20px,$x,$x,3px)
        height 2px
        width 9px
        background-color #BBB
        transform rotate(-42deg)
        transition 0.35s ease
    
.tab
    z-index 10
    height 180px
    width 65%
    max-width 1000px
    margin -90px auto
    padding 15px 30px
    background-color white
    border-radius 2px
    box-shadow 0px 3px 10px -1px rgba(0,0,0,0.5)
    text-align center
    &.menu-tab
        min-height 500px
        top 65%
        height auto
        margin auto
        max-width 800px
        z-index 300
        
.words-section
    height 480px
    overflow hidden
    & .content-container
        setup($x,absolute,$x,50%,80px,$x,450px)
        transform translateY(-50%)
        max-width 900px
    &.tea
        background url('https://s-media-cache-ak0.pinimg.com/564x/d0/33/98/d03398a1a49061d02a55bdb1924baa56.jpg') left no-repeat
        background-color #E7E7E7
    &.leaves
        background url('https://img0.etsystatic.com/010/0/5254040/il_570xN.440476162_heqm.jpg') left no-repeat
        background-size 450px
        background-color #FAFCF9
    &.right {background-position: right;}
    &.right .content-container {setup($x,absolute,$x,50%,450px,$x,auto); text-align: right; width: calc(100% - 530px);}
        
.large-section
    min-height 600px
    padding 80px 40px 40px
    background-color #FFDFD4
    text-align center
    font('Open Sans',16px,0px,400,26px)
    & h1 {margin-bottom: 40px}
    
.option-square
    min-height 360px
    width 280px
    margin 20px 2px
    padding 10px 15px
    vertical-align text-top
    border 1px solid black
    
.option-image
    height 180px
    width 180px
    margin 10px auto 20px
    background-color rgba(0,0,0,0.4)
    